import {Round} from "./round";
import {Term} from "./term";
import {Player} from "./player";

export interface Geodata {

  id: number;
  longitude: number;
  latitude: number;
  heading: number;
  pitch: number;
  zoom: number;
  player: Player;
  term: Term;
  round: Round;
  upvote: number;
  counterFlag: number;
  isFlagged: boolean;

}
