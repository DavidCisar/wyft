import {Player} from "./player";
import {Round} from "./round";

export interface Lobby {

  id: number;
  code: string;
  players: Player[];
  rounds: Round[];

}
