import {Round} from "./round";
import {Geodata} from "./geodata";

export interface Term {

  id: number;
  term: string;
  rounds: Round[];

}
