import { Lobby } from "./lobby";
import {Round} from "./round";

export interface Player {

  id: number;
  name: string;
  ipAddress: string;
  isActive: boolean;
  createdAt: string;
  lobby: Lobby;
  rounds: Round[];

}
