import {Player} from "./player";
import {Term} from "./term";
import {Round} from "./round";

export interface Saveposition {

  longitude: number;
  latitude: number;
  heading: number;
  pitch: number;
  zoom: number;

  playerId: number;
  termId: number;
  roundId: number;

}
