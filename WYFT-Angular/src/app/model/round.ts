import {Player} from "./player";
import {Term} from "./term";
import {Geodata} from "./geodata";

export interface Round {

  id: number;
  createdAt: string;
  votingAt: string;
  currentState: string
  timeLimitRound: number,
  timeLimitVoting: number,
  amountOfTerms: number,
  terms: Term[];
  players: Player[];
  playersVoted: Player[];
  geodata: Geodata[];

}
