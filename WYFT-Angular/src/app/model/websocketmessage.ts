export interface WebsocketMessage{
  message: string,
  color: string
}
