import {Component, OnInit, ViewChild} from '@angular/core';
import {Lobby} from "../../model/lobby";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  lobby?: Lobby;

  inputCode: string = "";
  showInputCode: boolean = false;

  showOptions: boolean = false;

  timeLimitRound?: any;
  tLRArray: number[] = [300, 600, 900, 1800, 3600];

  timeLimitVoting?: any;
  tLVArray: number[] = [120, 300, 600, 900];

  amountOfTerms?: any;
  aOTArray: number[] = [2, 3, 5, 8, 10];

  scrollValue: number = 0;

  constructor(private http: HttpClient, private router: Router, private location: Location) { }

  ngOnInit() {

    window.addEventListener("scroll", (event)=>{
      const navbar = document.getElementById("navbar");

      if (window.scrollY <= 0){
        this.scrollValue = 0;
      }else if (window.scrollY > 0 && window.scrollY < window.innerHeight){
        this.scrollValue = window.scrollY/window.innerHeight;
      }else{
        this.scrollValue = 1;
      }

      navbar!.style.backgroundColor = `rgba(255, 255, 255, ${this.scrollValue*0.2})`;
      navbar!.style.paddingTop = `${15*(1-this.scrollValue)+10}px`
    });

  }

  createLobby() {
    if (this.timeLimitRound != undefined && this.timeLimitVoting != undefined && this.amountOfTerms != undefined){

      let queryParams = new HttpParams();
      queryParams = queryParams.append("tlr", this.timeLimitRound!);
      queryParams = queryParams.append("tlv", this.timeLimitVoting!);
      queryParams = queryParams.append("aot", this.amountOfTerms!);
      this.http.get<Lobby>('/api/lobby/create', {params: queryParams})
        .subscribe(lob => this.redirectToLobby(lob.code));

    }else{

      alert("Please configure your game!")

    }
  }

  redirectToLobby(code: string){
    this.router.navigateByUrl("lobby?code=" + code).then((success: boolean) => console.log(success));
  }

  clickedJoin(){

    if (this.inputCode != ""){

      this.location.go("/creating")

      this.redirectToLobby(this.inputCode);

    }else{
      alert("Please enter a lobby code!")
    }

  }

  clickedCreate() {

    this.location.go("/creating")

    this.createLobby()

  }

}
