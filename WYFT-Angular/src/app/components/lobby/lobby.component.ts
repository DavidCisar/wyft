import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpParams } from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {Lobby} from "../../model/lobby";
import {Round} from "../../model/round";
import {Player} from "../../model/player";

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {Geodata} from "../../model/geodata";
import {Saveposition} from "../../model/saveposition";

interface Result {
  player : Player,
  score : number,
  totalUpvotes: number,
  totalFlags: number
}

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css']
})
export class LobbyComponent implements OnInit {

  overview: Result[] = [];

  lobby?: Lobby
  currentRound?: Round

  lobbyCode: string = "";

  you?: Player;

  ipAddress = {
    isInRound: false,
    isAdmin: false,
    address: '',
    voted: false
  };

  stompClient?: any;

  notification?: string;

  timeLeftRound?: number;
  tlrMin = this.timeLeftRound! / 60;
  tlrSec = this.timeLeftRound! % 60;

  timeLeftVoting?: number;
  tlvMin = this.timeLeftVoting! / 60;
  tlvSec = this.timeLeftVoting! % 60;

  interval?: number;
  countdownStarted: boolean = false;

  selectedTerm: string = '';

  allGeodata: Geodata[] = [];
  selectedGeodata?: Geodata;

  @ViewChild('map') mapElement: any;
  map?: google.maps.Map;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {

    this.http.get("http://api.ipify.org/?format=json")
      .subscribe((res: any) => {
        this.ipAddress.address = res.ip;

        this.route.queryParamMap.subscribe((value) => {

          this.lobbyCode = value.get("code") || "";

          //GET INFORMATION ABOUT LOBBY FROM CODE
          this.http.get<Lobby>("api/lobby?code=" + this.lobbyCode).subscribe((lobby: Lobby) => {
            this.lobby = lobby;
            this.currentRound = lobby.rounds[lobby.rounds.length - 1];

            // Get countdown if player connects after the game has started
            if (this.currentRound.currentState === 'STARTED') {
              this.timeLeftRound = new Date(Date.parse(this.currentRound.createdAt)).getSeconds()
                + this.currentRound.timeLimitRound - new Date().getSeconds();
              this.tlrMin = Math.floor(this.timeLeftRound! / 60);
              this.tlrSec = this.timeLeftRound! % 60;
              if (!this.countdownStarted) {
                this.startCountdown();
              }
            }

            // Get countdown for voting if player connects after the voting has started
            if (this.currentRound.currentState === 'VOTING') {
              this.timeLeftVoting = new Date(Date.parse(this.currentRound.votingAt)).getSeconds()
                + this.currentRound.timeLimitVoting - new Date().getSeconds();
              this.tlvMin = Math.floor(this.timeLeftVoting! / 60);
              this.tlvSec = this.timeLeftVoting! % 60;
              if (!this.countdownStarted) {
                this.startCountdown();
              }
            }

            //Check IP-ADDRESS
            this.checkIPAddress(this.ipAddress.address);

            //Check Score
            this.calculateScore();

            //CONNECT TO WEBSOCKET
            this.connectToSocket();

          });

        });
      })
  }

  checkIPAddress(address: string) {

    if (this.currentRound && this.currentRound.players.length > 0) {
      for (let player of this.currentRound.players) {
        if (player.ipAddress === address) {
          this.ipAddress.isInRound = true;
        }
      }
      if (this.currentRound.players[0].ipAddress === address) {
        this.ipAddress.isAdmin = true;
      }
    }

    console.log(this.currentRound);
    if (this.currentRound && this.currentRound?.playersVoted === undefined){
      this.currentRound!.playersVoted = [];
    }

    if (this.currentRound && this.currentRound!.playersVoted.length > 0) {
      for (let player of this.currentRound.playersVoted) {
        if (player.ipAddress === address) {
          this.ipAddress.voted = true;
        }
      }
    }

    //FIND YOU
    this.findYou();

  }

  checkPlayerName(playerName: string): boolean {
    for (let player of this.currentRound!.players) {
      if (player.name === playerName) {
        return true;
      }
    }
    return false;
  }

  createPlayer(ipAddress: string, playerName: string) {
    let newPlayer: Player = {
      id: 0,
      name: playerName,
      ipAddress: ipAddress,
      lobby: this.lobby!,
      isActive: true,
      rounds: [],
      createdAt: 'simpleString'
    }

    if (!this.checkPlayerName(playerName)) {
      this.currentRound?.players.push(newPlayer);

      if (this.stompClient) {

        this.stompClient.send(
          "/ws/lobby/app/" + this.lobbyCode + "/createPlayer",
          {},
          JSON.stringify(this.currentRound!)
        )
      } else {
        alert("Something went wrong...")
      }
    }

  }

  addPlayerLobby(player: Player) {
    this.lobby!.players.push(player);
    this.currentRound!.players.push(player);
  }

  deletePlayer(){
    this.findYou();
    let index: number;
    for (let i = 0; i < this.currentRound!.players.length; i++){
      if(this.you?.id === this.currentRound!.players[i].id){
        index = i;
      }
    }
    this.currentRound?.players.splice(index!,1);
    this.ipAddress.isInRound = false;

    //Synconisation over Websocet
    if (this.stompClient) {

      this.stompClient.send(
        "/ws/lobby/app/" + this.lobbyCode + "/deletePlayer/" + this.you?.id,
        {},
        JSON.stringify(this.currentRound!)
      )
    } else {
      alert("Something went wrong...")
    }
  }

  startGame() {
    if (this.stompClient) {

      this.stompClient.send(
        "/ws/lobby/app/" + this.lobbyCode + "/start",
        {},
        JSON.stringify(this.currentRound!)
      )

    } else {
      alert("Something went wrong...")
    }
    this.timeLeftRound = this.currentRound!.timeLimitRound;
    if (!this.countdownStarted) {
      this.startCountdown();
    }
  }

  startCountdown() {
    this.countdownStarted = true;
    this.interval = setInterval(() => {
      switch(this.currentRound!.currentState) {
        case 'STARTED':
          if (this.timeLeftRound! > 0) {
            this.timeLeftRound!--;
            this.tlrMin = Math.floor(this.timeLeftRound! / 60);
            this.tlrSec = this.timeLeftRound! % 60;
          } else {
            this.endGame();
          }
          break;
        case 'VOTING':
          if (this.timeLeftVoting! > 0) {
            this.timeLeftVoting!--;
            this.tlvMin = Math.floor(this.timeLeftVoting! / 60);
            this.tlvSec = this.timeLeftVoting! % 60;
          } else {
            if (!this.ipAddress.voted) {
              this.endVoting();
            }
          }
          break;
      }
    }, 1000)
  }

  changeRound() {
    //CHANGE THE ROUND FOR EVERYBODY IN THE LOBBY
  }

  endGame(){
    this.timeLeftVoting = this.currentRound!.timeLimitVoting;
    if (this.stompClient) {
      this.stompClient.send(
        "/ws/lobby/app/" + this.lobbyCode + "/end",
        {},
        JSON.stringify(this.currentRound!)
      )
    } else {
      alert("Something went wrong...")
    }
  }

  // VOTING & FLAG FUNCTIONALITY
  giveUpVote(geodataId: number) {
    for (let geodata of this.currentRound!.geodata) {
      if (geodata.id === geodataId) {
        geodata.upvote = 1;
      }
    }
  }

  setFlag(geodataId: number) {
    for (let geodata of this.currentRound!.geodata) {
      if (geodata.id === geodataId) {
        geodata.isFlagged = true;
        geodata.counterFlag = 1;
      }
    }
  }

  endVoting(){
    this.findYou();
    if (this.stompClient) {

      this.stompClient.send(
        "/ws/lobby/app/" + this.lobbyCode + "/voting/" + this.you!.id,
        {},
        JSON.stringify(this.currentRound!)
        )

      } else {
      alert("Something went wrong...")
    }
    this.calculateScore();
  }

  connectToSocket(){

    //CONNECT TO SPRING WEBSOCKET
    const socket = new SockJS("/ws/lobby/stomp"); // INITIALIZES THE CONNECTION
    this.stompClient = Stomp.over(socket);

    this.stompClient.connect({}, (frame: any) => {

      this.stompClient.subscribe(("/ws/lobby/topic/" + this.lobbyCode + "/start"), (frame: any) => {

        this.currentRound = JSON.parse(frame.body)!;
        this.lobby!.rounds[this.lobby!.rounds.length - 1] = this.currentRound!;
        if (!this.countdownStarted) {
          this.startCountdown();
        }

      });

      this.stompClient.subscribe(("/ws/lobby/topic/" + this.lobbyCode + "/message"), (frame: any) => {

        const message = JSON.parse(frame.body);
        this.notification = message.message;

        setTimeout(() => {
          this.notification = undefined;
        }, 4000)

      })

      this.stompClient.subscribe(("/ws/lobby/topic/" + this.lobbyCode + "/end"), (frame: any) => {

        this.currentRound = JSON.parse(frame.body)!;

        this.allGeodata = this.currentRound!.geodata;
        console.log("geodata: " + JSON.stringify(this.allGeodata));

        this.lobby!.rounds[this.lobby!.rounds.length - 1] = this.currentRound!;

        if (!this.countdownStarted) {
          this.startCountdown();
        }
      });

      this.stompClient.subscribe(("/ws/lobby/topic/" + this.lobbyCode + "/voting"), (frame: any) => {

        this.currentRound = JSON.parse(frame.body)!;
        this.lobby!.rounds[this.lobby!.rounds.length - 1] = this.currentRound!;
        this.checkIPAddress(this.ipAddress.address);
        this.calculateScore();

      });

      this.stompClient.subscribe(("/ws/lobby/topic/" + this.lobbyCode + "/createPlayer"), (frame: any) => {

        this.currentRound = JSON.parse(frame.body)!;
        this.lobby!.rounds[this.lobby!.rounds.length - 1] = this.currentRound!;
        this.checkIPAddress(this.ipAddress.address);
        this.findYou();
      })


      this.stompClient.subscribe(("/ws/lobby/topic/" + this.lobbyCode + "/deletePlayer"), (frame: any) => {

        this.currentRound = JSON.parse(frame.body)!;
        this.lobby!.rounds[this.lobby!.rounds.length - 1] = this.currentRound!;
        for (let player of this.currentRound!.players) {
          if (player.ipAddress === this.you?.ipAddress) {
            this.checkIPAddress(this.ipAddress.address);
            return;
          }
        }
        this.router.navigateByUrl('');
      });

      this.http.get("http://api.ipify.org/?format=json")
        .subscribe((res: any) => {
          console.log(res.ip);
          this.ipAddress.address = res.ip;
          this.http.get<Round>("/api/round?id=" + this.currentRound!.id).subscribe((round: Round) => {
            this.currentRound = round;
            this.findYou();
          })

        });

    })
  }

  savePosition(saveposition: Saveposition) {

    console.log(saveposition);

    this.stompClient.send(
      "/ws/lobby/app/" + this.lobbyCode + "/found",
      {},
      JSON.stringify(saveposition)
    )
  }

  findYou() {
    const arr = this.currentRound!.players.filter(player => player.ipAddress == this.ipAddress.address);
    if (arr.length > 0) {
      this.you = arr[0]
    }
    console.log(this.you);
  }

  getSavedPositions() {

  }

  changeSelected(geodata: Geodata){
    this.selectedGeodata = geodata;
    console.log("change Selected");

    setTimeout(() => {

      const position = {
        lat: this.selectedGeodata?.latitude || 0,
        lng: this.selectedGeodata?.longitude || 0
      };
      const map = new google.maps.Map(
        this.mapElement.nativeElement,
        {
          center: position
        }
      );
      const panorama = new google.maps.StreetViewPanorama(
        this.mapElement.nativeElement,
        {
          position: position,
          pov: {
            heading: this.selectedGeodata?.heading,
            pitch: this.selectedGeodata?.pitch,
          },
          zoom: this.selectedGeodata?.zoom,
          disableDefaultUI: true,
        }
      );

      map.setStreetView(panorama);

    }, 100);

  }


  calculateScore(){
    this.overview = [];
    for (let player of this.currentRound!.players){
      const geodataArray : Geodata[] = this.currentRound?.geodata.filter(geodata => geodata.player.id === player.id) || []
      let playerScore = 0;
      let totalUpvotes = 0;
      let totalFlags = 0;
      for (let geoData of geodataArray){
        playerScore += geoData.upvote * 25;
        playerScore -= geoData.counterFlag * 35;
        playerScore += 10;
        totalUpvotes += geoData.upvote;
        totalFlags += geoData.counterFlag;
      }
      this.overview.push({
        player : player,
        score : playerScore,
        totalUpvotes : totalUpvotes,
        totalFlags : totalFlags
      })
    }
  }

  closeMap(){
    this.selectedGeodata = undefined;
  }

  secondsToMinuteString(seconds: number): string{

    const minutes = Math.floor(seconds / 60);
    const remaining = seconds % 60;

    return `${minutes}:${remaining < 10 ? '0':''}${remaining}`;

  }

}
