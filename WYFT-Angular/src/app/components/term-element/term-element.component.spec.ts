import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TermElementComponent } from './term-element.component';

describe('TermElementComponent', () => {
  let component: TermElementComponent;
  let fixture: ComponentFixture<TermElementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TermElementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TermElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
