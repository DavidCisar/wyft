import {Component, Input, OnInit} from '@angular/core';
import {Term} from "../../model/term";

@Component({
  selector: 'app-term-element',
  templateUrl: './term-element.component.html',
  styleUrls: ['./term-element.component.css']
})
export class TermElementComponent implements OnInit {

  @Input() term!: Term;

  constructor() { }

  ngOnInit(): void {
  }

}
