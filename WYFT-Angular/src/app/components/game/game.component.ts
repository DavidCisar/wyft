import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {} from 'googlemaps';
import {Round} from "../../model/round";
import {Term} from "../../model/term";
import {Geodata} from "../../model/geodata";
import {Player} from "../../model/player";
import {Saveposition} from "../../model/saveposition";
import MapOptions = google.maps.MapOptions;

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  @Input() round?: Round;
  @Input() player?: Player;
  selectedTerm?: number;
  @Input() timeLeftRound?: number;

  @Output() savePositionEvent = new EventEmitter<Saveposition>();

  savedPositions: Saveposition[] = [];

  @ViewChild('mapId') mapElement: any;
  map?: google.maps.Map;

  constructor() { }

  ngOnInit(): void {

    console.log("spawn maps")

    if (this.round && this.round!.terms.length > 0){
      this.selectedTerm = this.round!.terms[0].id;
    }

  }

  ngAfterViewInit(): void{

    const mapProperties: MapOptions = {
      center: new google.maps.LatLng(35.2271, -80.8431),
      zoom: 3,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      streetViewControl: true,
      disableDefaultUI: true,
      styles: [{
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [{
          "saturation": 36
        }, {
          "color": "#000000"
        }, {
          "lightness": 40
        }]
      }, {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [{
          "visibility": "on"
        }, {
          "color": "#000000"
        }, {
          "lightness": 16
        }]
      }, {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 20
        }]
      }, {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 17
        }, {
          "weight": 1.2
        }]
      }, {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "administrative.country",
        "elementType": "all",
        "stylers": [{
          "visibility": "simplified"
        }]
      }, {
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [{
          "visibility": "simplified"
        }]
      }, {
        "featureType": "administrative.country",
        "elementType": "labels.text",
        "stylers": [{
          "visibility": "simplified"
        }]
      }, {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "administrative.locality",
        "elementType": "all",
        "stylers": [{
          "visibility": "simplified"
        }, {
          "saturation": -100
        }, {
          "lightness": 30
        }]
      }, {
        "featureType": "administrative.neighborhood",
        "elementType": "all",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "administrative.land_parcel",
        "elementType": "all",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [{
          "visibility": "simplified"
        }, {
          "gamma": 0.00
        }, {
          "lightness": 74
        }]
      }, {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 20
        }]
      }, {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [{
          "lightness": 3
        }]
      }, {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 21
        }]
      }, {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [{
          "visibility": "simplified"
        }]
      }, {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 17
        }]
      }, {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 29
        }, {
          "weight": 0.2
        }]
      }, {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 18
        }]
      }, {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 16
        }]
      }, {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 19
        }]
      }, {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
          "lightness": 17
        }]
      }]
    };
    console.log("Starting Maps...")
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

  }

  selectTerm(term: Term){

    const index = this.round!.terms.indexOf(term, 0) ?? 0;
    this.selectedTerm = this.round!.terms[index].id;

  }

  savePosition(){

    if (this.map?.getStreetView().getVisible()){

      const term = this.round!.terms.filter(term => term.id == this.selectedTerm)[0]

      const geoData: Saveposition = {
        longitude: this.map?.getStreetView().getPosition().lng(),
        latitude: this.map?.getStreetView().getPosition().lat(),
        heading: this.map?.getStreetView().getPov().heading || 0,
        pitch: this.map?.getStreetView().getPov().pitch || 0,
        zoom: this.map?.getStreetView().getZoom() || 0,
        playerId: this.player?.id ?? 0,
        termId: term.id,
        roundId: this.round!.id
      };

      this.savedPositions.push(geoData);

      this.savePositionEvent.next(geoData);

    }else{
      alert("You have to be in streetview to save a position!")
    }

  }

  savedToTerm(term: Term): boolean{
    return this.savedPositions.filter(position => position.termId === term.id).length > 0;
  }

  secondsToMinuteString(seconds: number): string{

    const minutes = Math.floor(seconds / 60);
    const remaining = seconds % 60;

    return `${minutes}:${remaining < 10 ? '0':''}${remaining}`;

  }

}
