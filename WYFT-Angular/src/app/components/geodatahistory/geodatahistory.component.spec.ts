import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeodatahistoryComponent } from './geodatahistory.component';

describe('GeodatahistoryComponent', () => {
  let component: GeodatahistoryComponent;
  let fixture: ComponentFixture<GeodatahistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeodatahistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeodatahistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
