import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Geodata } from 'src/app/model/geodata';
import {Term} from "../../model/term";

@Component({
  selector: 'app-geodatahistory',
  templateUrl: './geodatahistory.component.html',
  styleUrls: ['./geodatahistory.component.css']
})
export class GeodatahistoryComponent implements OnInit {

  allGeoData: Geodata[] = [];
  selectedGeodata?: Geodata;

  @ViewChild('map') mapElement: any;
  map?: google.maps.Map;

  allTerms: Term[] = [];
  selectedTerm?: string;
  filter: boolean = false;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get<Geodata[]>("/api/geodata/history")
      .subscribe(geodataList => {
        this.allGeoData = geodataList;
        if (this.allGeoData.length > 0) {
          this.changeSelected(this.allGeoData[0]);
        }
      });

    this.http.get<Term[]>("/api/term/getAllTerms")
      .subscribe({
        next: termList => {
          this.allTerms = termList;
        },
        error: err => console.log(err)
      });

  }

  changeSelected(geodata: Geodata){
    this.selectedGeodata = geodata;
    console.log("change Selected");

    setTimeout(() => {

      const position = {
        lat: this.selectedGeodata?.latitude || 0,
        lng: this.selectedGeodata?.longitude || 0
      };
      const map = new google.maps.Map(
        this.mapElement.nativeElement,
        {
          center: position
        }
      );
      const panorama = new google.maps.StreetViewPanorama(
        this.mapElement.nativeElement,
        {
          position: position,
          pov: {
            heading: this.selectedGeodata?.heading,
            pitch: this.selectedGeodata?.pitch,
          },
          zoom: this.selectedGeodata?.zoom,
          disableDefaultUI: true,
        }
      );

      map.setStreetView(panorama);

    }, 100);

  }

  setFilter() {
    if (this.selectedTerm != undefined) {
      this.selectedTerm = undefined;
    }
    this.filter = !this.filter;
  }

  clickedShowAll(){
    alert("Under construction...");
  }
}
