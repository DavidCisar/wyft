import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeodataoverviewComponent } from './geodataoverview.component';

describe('GeodataoverviewComponent', () => {
  let component: GeodataoverviewComponent;
  let fixture: ComponentFixture<GeodataoverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeodataoverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeodataoverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
