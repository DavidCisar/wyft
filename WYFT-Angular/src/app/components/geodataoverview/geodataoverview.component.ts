import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Geodata} from "../../model/geodata";

@Component({
  selector: 'app-geodataoverview',
  templateUrl: './geodataoverview.component.html',
  styleUrls: ['./geodataoverview.component.css']
})
export class GeodataoverviewComponent implements OnInit {

  @Input() votable: boolean = true;
  @Input() geodata?: Geodata[];

  terms: GeodataCollection[] = [];

  selectedGeodata?: Geodata;

  @ViewChild('map') mapElement: any;
  map?: google.maps.Map;


  constructor() { }

  ngOnInit(): void {
    this.sortByTerms();
  }

  sortByTerms(){

    for (let datum of this.geodata!){

      //EXISTS
      if (this.terms.filter(term => term.name == datum.term.term).length > 0){

        const index = this.terms.findIndex(term => term.name == datum.term.term);
        this.terms[index].geodata.push(datum);

      }else{

        this.terms.push({
          name: datum.term.term,
          geodata: [datum]
        })

      }

    }

  }

  changeSelected(geodata: Geodata){
    this.selectedGeodata = geodata;
    console.log("change Selected");

    setTimeout(() => {

      const position = {
        lat: this.selectedGeodata?.latitude || 0,
        lng: this.selectedGeodata?.longitude || 0
      };
      const map = new google.maps.Map(
        this.mapElement.nativeElement,
        {
          center: position
        }
      );
      const panorama = new google.maps.StreetViewPanorama(
        this.mapElement.nativeElement,
        {
          position: position,
          pov: {
            heading: this.selectedGeodata?.heading,
            pitch: this.selectedGeodata?.pitch,
          },
          zoom: this.selectedGeodata?.zoom,
          disableDefaultUI: true,
        }
      );

      map.setStreetView(panorama);

    }, 100);

  }

}

interface GeodataCollection{
  name: string;
  geodata: Geodata[];
}
