import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Geodata} from "../../model/geodata";

@Component({
  selector: 'app-geodatapreview',
  templateUrl: './geodatapreview.component.html',
  styleUrls: ['./geodatapreview.component.css']
})
export class GeodatapreviewComponent implements OnInit {

  @Input() votable?: boolean;
  @Input() geodata?: Geodata;
  @Input() showTerm: boolean = false;

  @Output() changeSelected = new EventEmitter<Geodata>();


  constructor() { }

  ngOnInit(): void {
  }

  emitChange(){
    this.changeSelected.next(this.geodata!);
  }

}
