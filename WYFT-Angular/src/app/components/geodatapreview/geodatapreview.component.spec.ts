import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeodatapreviewComponent } from './geodatapreview.component';

describe('GeodatapreviewComponent', () => {
  let component: GeodatapreviewComponent;
  let fixture: ComponentFixture<GeodatapreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeodatapreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeodatapreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
