import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { LobbyComponent } from './components/lobby/lobby.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { FormsModule} from "@angular/forms";
import { GameComponent } from './components/game/game.component';
import { GeodatapreviewComponent } from './components/geodatapreview/geodatapreview.component';
import { GeodataoverviewComponent } from './components/geodataoverview/geodataoverview.component';
import { GeodatahistoryComponent } from './components/geodatahistory/geodatahistory.component';
import {ClipboardModule} from "ngx-clipboard";

@NgModule({
  declarations: [
    AppComponent,
    LobbyComponent,
    HomepageComponent,
    GameComponent,
    GeodatapreviewComponent,
    GeodataoverviewComponent,
    GeodatahistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ClipboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
