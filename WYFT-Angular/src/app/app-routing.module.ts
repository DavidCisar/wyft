import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LobbyComponent } from "./components/lobby/lobby.component";
import { HomepageComponent } from './components/homepage/homepage.component';
import { GeodatahistoryComponent } from './components/geodatahistory/geodatahistory.component';

const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'lobby', component: LobbyComponent},
  {path: 'history', component: GeodatahistoryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
