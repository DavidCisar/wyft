package com.abschlussprojekt.wyftspring.player;

import com.abschlussprojekt.wyftspring.lobby.Lobby;
import com.abschlussprojekt.wyftspring.lobby.LobbyRepository;
import com.abschlussprojekt.wyftspring.lobby.LobbyService;
import com.abschlussprojekt.wyftspring.round.RoundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class PlayerServiceImpl implements PlayerService{

    private final PlayerRepository playerRepository;
    private final LobbyService lobbyService;
    private final RoundService roundService;


    @Autowired
    public PlayerServiceImpl(PlayerRepository playerRepository, LobbyService lobbyService, RoundService roundService) {
        this.playerRepository = playerRepository;
        this.lobbyService = lobbyService;
        this.roundService = roundService;
    }

    @Override
    public Player createPlayer(PlayerDTO playerDTO) {
        Player newPlayer = new Player(playerDTO.getName(), playerDTO.getIpAddress(), true, Instant.now());
        return playerRepository.save(newPlayer);
    }

    @Override
    public void deletePlayer(Long id) {
        Player player = playerRepository.findById(id).orElseThrow();;
        playerRepository.delete(player);
    }

    public static PlayerDTO mapPlayerToDTO(Player player){
        return new PlayerDTO(
                player.getId(),
                player.getName(),
                player.getIpAddress(),
                player.getActive(),
                player.getCreatedAt().toString()
        );
    }

}
