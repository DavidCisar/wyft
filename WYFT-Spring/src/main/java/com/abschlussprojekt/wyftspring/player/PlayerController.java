package com.abschlussprojekt.wyftspring.player;

import com.abschlussprojekt.wyftspring.lobby.LobbyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/player")
public class PlayerController {

    private final PlayerService playerService;
    private final LobbyService lobbyService;

    @Autowired
    public PlayerController(PlayerService playerService, LobbyService lobbyService) {
        this.playerService = playerService;
        this.lobbyService = lobbyService;
    }
}
