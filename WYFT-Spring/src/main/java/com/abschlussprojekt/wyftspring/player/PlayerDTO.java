package com.abschlussprojekt.wyftspring.player;

import com.abschlussprojekt.wyftspring.lobby.LobbyDTO;
import com.abschlussprojekt.wyftspring.round.RoundDTO;

import java.util.List;

public class PlayerDTO {

    private Long id;
    private String name;
    private String ipAddress;
    private Boolean isActive;
    private String createdAt;

    public PlayerDTO(
            Long id,
            String name,
            String ipAddress,
            Boolean isActive,
            String createdAt){
        this.id = id;
        this.name = name;
        this.ipAddress = ipAddress;
        this.isActive = isActive;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
