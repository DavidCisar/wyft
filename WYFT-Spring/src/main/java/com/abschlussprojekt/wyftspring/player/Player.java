package com.abschlussprojekt.wyftspring.player;

import com.abschlussprojekt.wyftspring.lobby.Lobby;
import com.abschlussprojekt.wyftspring.round.Round;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Player {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String ipAddress;
    private Boolean isActive;
    private Instant createdAt;

    @ManyToOne
    private Lobby lobby;

    @ManyToMany
    private List<Round> rounds;

    public Player() {}

    public Player(String name, String ipAddress, Boolean isActive, Instant createdAt) {
        this.name = name;
        this.ipAddress = ipAddress;
        this.isActive = isActive;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }
}
