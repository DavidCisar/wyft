package com.abschlussprojekt.wyftspring.player;

import java.util.Optional;

public interface PlayerService {
    Player createPlayer(PlayerDTO playerDTO);
    void deletePlayer (Long id);
}
