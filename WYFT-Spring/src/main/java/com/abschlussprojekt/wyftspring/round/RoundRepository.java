package com.abschlussprojekt.wyftspring.round;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoundRepository extends JpaRepository<Round, Long> {

    Optional<Round> findRoundById(Long id);
}
