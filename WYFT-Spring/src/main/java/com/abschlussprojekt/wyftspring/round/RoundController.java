package com.abschlussprojekt.wyftspring.round;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/round")
public class RoundController {

    private final RoundService roundService;

    @Autowired
    public RoundController(RoundService roundService) {
        this.roundService = roundService;
    }

    @GetMapping
    public ResponseEntity<RoundDTO> findById(@RequestParam Long id){
        RoundDTO dto = roundService.findById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}
