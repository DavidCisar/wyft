package com.abschlussprojekt.wyftspring.round;

import com.abschlussprojekt.wyftspring.geodata.GeodataDTO;
import com.abschlussprojekt.wyftspring.player.PlayerDTO;
import com.abschlussprojekt.wyftspring.term.TermDTO;

import java.util.List;

public class RoundDTO {

    private Long id;
    private String createdAt;
    private String votingAt;
    private String currentState;
    private int timeLimitRound;
    private int timeLimitVoting;
    private int amountOfTerms;
    private PlayerDTO admin;
    private List<PlayerDTO> players;
    private List<PlayerDTO> playersVoted;
    private List<TermDTO> terms;
    private List<GeodataDTO> geodata;

    public RoundDTO(
            Long id,
            String createdAt,
            String votingAt,
            String currentState,
            int timeLimitRound,
            int timeLimitVoting,
            int amountOfTerms,
            PlayerDTO admin,
            List<PlayerDTO> players,
            List<PlayerDTO> playersVoted,
            List<TermDTO> terms,
            List<GeodataDTO> geodata){
        this.id = id;
        this.createdAt = createdAt;
        this.votingAt = votingAt;
        this.currentState = currentState;
        this.timeLimitRound = timeLimitRound;
        this.timeLimitVoting = timeLimitVoting;
        this.amountOfTerms = amountOfTerms;
        this.admin = admin;
        this.players = players;
        this.playersVoted = playersVoted;
        this.terms = terms;
        this.geodata = geodata;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getVotingAt() {
        return votingAt;
    }

    public void setVotingAt(String votingAt) {
        this.votingAt = votingAt;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public int getTimeLimitRound() {
        return timeLimitRound;
    }

    public void setTimeLimitRound(int timeLimitRound) {
        this.timeLimitRound = timeLimitRound;
    }

    public int getTimeLimitVoting() {
        return timeLimitVoting;
    }

    public void setTimeLimitVoting(int timeLimitVoting) {
        this.timeLimitVoting = timeLimitVoting;
    }

    public int getAmountOfTerms() {
        return amountOfTerms;
    }

    public void setAmountOfTerms(int amountOfTerms) {
        this.amountOfTerms = amountOfTerms;
    }

    public PlayerDTO getAdmin() {
        return admin;
    }

    public void setAdmin(PlayerDTO admin) {
        this.admin = admin;
    }

    public List<PlayerDTO> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerDTO> players) {
        this.players = players;
    }

    public List<PlayerDTO> getPlayersVoted() {
        return playersVoted;
    }

    public void setPlayersVoted(List<PlayerDTO> playersVoted) {
        this.playersVoted = playersVoted;
    }

    public List<TermDTO> getTerms() {
        return terms;
    }

    public void setTerms(List<TermDTO> terms) {
        this.terms = terms;
    }

    public List<GeodataDTO> getGeodata() {
        return geodata;
    }

    public void setGeodata(List<GeodataDTO> geodata) {
        this.geodata = geodata;
    }
}
