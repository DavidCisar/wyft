package com.abschlussprojekt.wyftspring.round;

import com.abschlussprojekt.wyftspring.State;
import com.abschlussprojekt.wyftspring.geodata.Geodata;
import com.abschlussprojekt.wyftspring.lobby.Lobby;
import com.abschlussprojekt.wyftspring.player.Player;
import com.abschlussprojekt.wyftspring.term.Term;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Entity
public class Round {

    @Id
    @GeneratedValue
    private Long id;
    private Instant createdAt;
    private Instant votingAt;
    private State currentState = State.NOTSTARTED;
    private int timeLimitRound;
    private int timeLimitVoting;
    private int amountOfTerms;

    @OneToOne
    private Player admin;

    @ManyToMany
    private List<Player> playersVoted;

    @ManyToMany
    private List<Player> players;

    @ManyToOne
    private Lobby lobby;

    @ManyToMany
    private List<Term> terms;

    @OneToMany(mappedBy = "round")
    private List<Geodata> geodata;

    public Round() {}

    public Round(
            Instant createdAt,
            Instant votingAt,
            int timeLimitRound,
            int timeLimitVoting,
            int amountOfTerms,
            Player admin,
            List<Player> playersVoted,
            List<Player> players,
            Lobby lobby,
            List<Term> terms,
            List<Geodata> geodata) {
        this.createdAt = createdAt;
        this.votingAt = votingAt;
        this.timeLimitRound = timeLimitRound;
        this.timeLimitVoting = timeLimitVoting;
        this.amountOfTerms = amountOfTerms;
        this.admin = admin;
        this.playersVoted = playersVoted;
        this.players = players;
        this.lobby = lobby;
        this.terms = terms;
        this.geodata = geodata;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getVotingAt() {
        return votingAt;
    }

    public void setVotingAt(Instant votingAt) {
        this.votingAt = votingAt;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }

    public int getTimeLimitRound() {
        return timeLimitRound;
    }

    public void setTimeLimitRound(int timeLimitRound) {
        this.timeLimitRound = timeLimitRound;
    }

    public int getTimeLimitVoting() {
        return timeLimitVoting;
    }

    public void setTimeLimitVoting(int timeLimitVoting) {
        this.timeLimitVoting = timeLimitVoting;
    }

    public int getAmountOfTerms() {
        return amountOfTerms;
    }

    public void setAmountOfTerms(int amountOfTerms) {
        this.amountOfTerms = amountOfTerms;
    }

    public Player getAdmin() {
        return admin;
    }

    public void setAdmin(Player admin) {
        this.admin = admin;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Player> getPlayersVoted() {
        return playersVoted;
    }

    public void setPlayersVoted(List<Player> playersVoted) {
        this.playersVoted = playersVoted;
    }

    public Lobby getLobby() {
        return lobby;
    }

    public void setLobby(Lobby lobby) {
        this.lobby = lobby;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    public List<Geodata> getGeodata() {
        return geodata;
    }

    public void setGeodata(List<Geodata> geodata) {
        this.geodata = geodata;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public void addPlayerVoted(Player player) {
        this.playersVoted.add(player);
    }

    public void addGeodata(Geodata geodata){
        this.geodata.add(geodata);
    }

    @Override
    public String toString() {
        return "Round{" +
                "id=" + id +
                ", createdAt=" + createdAt +
                ", votingAt=" + votingAt +
                ", currentState=" + currentState +
                ", players=" + players +
                ", playersVoted=" + playersVoted +
                ", terms=" + terms +
                ", geodata=" + geodata +
                '}';
    }
}
