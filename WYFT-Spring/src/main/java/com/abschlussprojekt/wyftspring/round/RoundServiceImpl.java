package com.abschlussprojekt.wyftspring.round;

import com.abschlussprojekt.wyftspring.geodata.GeodataServiceImpl;
import com.abschlussprojekt.wyftspring.player.Player;
import com.abschlussprojekt.wyftspring.player.PlayerRepository;
import com.abschlussprojekt.wyftspring.player.PlayerServiceImpl;
import com.abschlussprojekt.wyftspring.term.TermServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.stream.Collectors;

@Service
public class RoundServiceImpl implements RoundService{

    private final RoundRepository roundRepository;
    private final PlayerRepository playerRepository;

    @Autowired
    public RoundServiceImpl(RoundRepository roundRepository, PlayerRepository playerRepository) {
        this.roundRepository = roundRepository;
        this.playerRepository = playerRepository;
    }

    @Override
    public Round pushPlayerToRound(Long roundId, Player newPlayer) {
        Round round = roundRepository.findRoundById(roundId).orElseThrow();
        if (round.getPlayers().size() == 0) {
            round.setAdmin(newPlayer);
        }
        round.addPlayer(newPlayer);
        return roundRepository.save(round);
    }

    public static RoundDTO mapRoundToDTO(Round round) {
        return new RoundDTO(
                round.getId(),
                round.getCreatedAt().toString(),
                round.getVotingAt().toString(),
                round.getCurrentState().name(),
                round.getTimeLimitRound(),
                round.getTimeLimitVoting(),
                round.getAmountOfTerms(),
                PlayerServiceImpl.mapPlayerToDTO(round.getAdmin()),
                round.getPlayers().stream().map(PlayerServiceImpl::mapPlayerToDTO).collect(Collectors.toList()),
                round.getPlayersVoted().stream().map(PlayerServiceImpl::mapPlayerToDTO).collect(Collectors.toList()),
                round.getTerms().stream().map(TermServiceImpl::mapTermToDTO).collect(Collectors.toList()),
                round.getGeodata().stream().map(GeodataServiceImpl::mapGeodataToDTO).collect(Collectors.toList())
        );
    }

    @Override
    public RoundDTO findById(Long id) {
        Round round = roundRepository.findById(id).orElseThrow();
        return RoundServiceImpl.mapRoundToDTO(round);
    }

    public Round deletePlayerFromRound(Round round, Long playerId){
        int index = -1;
        for (int i = 0; i < round.getPlayers().size(); i++) {
            if (playerId ==  round.getPlayers().get(i).getId()){
                index = i;
            }
        }
        if (index >= 0) {
            round.getPlayers().remove(index);
            if (index == 0 && round.getPlayers().size() == 0) {
                Player newAdmin = playerRepository.save(new Player("admin", "ipAddress-Placeholder", true, Instant.now()));
                round.setAdmin(newAdmin);
            } else if (index == 0 && round.getPlayers().size() > 0) {
                round.setAdmin(round.getPlayers().get(0));
            }
        }
        return round;
    }
}
