package com.abschlussprojekt.wyftspring.round;

import com.abschlussprojekt.wyftspring.player.Player;

public interface RoundService {
    RoundDTO findById(Long id);
    Round pushPlayerToRound(Long roundId, Player newPlayer);
    Round deletePlayerFromRound(Round round, Long playerId);
}
