package com.abschlussprojekt.wyftspring.websocket;

import com.abschlussprojekt.wyftspring.geodata.Geodata;
import com.abschlussprojekt.wyftspring.round.RoundDTO;
import com.abschlussprojekt.wyftspring.websocket.dto.SavepositionDTO;

public interface WebSocketService {
    RoundDTO startRound(RoundDTO roundDTO);

    Geodata savePosition(SavepositionDTO savepositionDTO);

    RoundDTO endRound(RoundDTO roundDTO);

    RoundDTO voted(RoundDTO roundDTO, Long playerId);

    RoundDTO createPlayer(String code, RoundDTO roundDTO);

    RoundDTO deletePlayer(RoundDTO roundDTO, Long playerId);
}
