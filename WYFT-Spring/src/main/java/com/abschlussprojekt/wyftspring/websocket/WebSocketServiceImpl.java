package com.abschlussprojekt.wyftspring.websocket;

import com.abschlussprojekt.wyftspring.State;
import com.abschlussprojekt.wyftspring.geodata.Geodata;
import com.abschlussprojekt.wyftspring.geodata.GeodataDTO;
import com.abschlussprojekt.wyftspring.geodata.GeodataRepository;
import com.abschlussprojekt.wyftspring.geodata.GeodataService;
import com.abschlussprojekt.wyftspring.lobby.Lobby;
import com.abschlussprojekt.wyftspring.lobby.LobbyRepository;
import com.abschlussprojekt.wyftspring.lobby.LobbyService;
import com.abschlussprojekt.wyftspring.player.Player;
import com.abschlussprojekt.wyftspring.player.PlayerRepository;
import com.abschlussprojekt.wyftspring.player.PlayerService;
import com.abschlussprojekt.wyftspring.round.*;
import com.abschlussprojekt.wyftspring.term.Term;
import com.abschlussprojekt.wyftspring.term.TermRepository;
import com.abschlussprojekt.wyftspring.websocket.dto.SavepositionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WebSocketServiceImpl implements WebSocketService{

    private final LobbyRepository lobbyRepository;
    private final RoundRepository roundRepository;
    private final TermRepository termRepository;
    private final PlayerRepository playerRepository;
    private final GeodataRepository geodataRepository;

    private final GeodataService geodataService;
    private final PlayerService playerService;
    private final RoundService roundService;
    private final LobbyService lobbyService;

    @Autowired
    public WebSocketServiceImpl(LobbyRepository lobbyRepository, RoundRepository roundRepository,
                                TermRepository termRepository, PlayerRepository playerRepository,
                                GeodataRepository geodataRepository, GeodataService geodataService,
                                PlayerService playerService, RoundService roundService,
                                LobbyService lobbyService) {
        this.roundRepository = roundRepository;
        this.termRepository = termRepository;
        this.lobbyRepository = lobbyRepository;
        this.playerRepository = playerRepository;
        this.geodataRepository = geodataRepository;
        this.geodataService = geodataService;
        this.playerService = playerService;
        this.roundService = roundService;
        this.lobbyService = lobbyService;
    }

    @Override
    @Transactional
    public RoundDTO startRound(RoundDTO roundDTO) {

        Round round = roundRepository.findById(roundDTO.getId()).orElseThrow();
        round.setCurrentState(State.STARTED);
        round.setCreatedAt(Instant.now());


        round.setGeodata(new ArrayList<>());

        round = roundRepository.save(round);

        RoundDTO roundDTO1 = RoundServiceImpl.mapRoundToDTO(round);
        System.out.println("roundDTO1 = " + roundDTO1);

        return roundDTO1;
    }

    @Override
    @Transactional
    public RoundDTO endRound(RoundDTO roundDTO) {

        Round round = roundRepository.findById(roundDTO.getId()).orElseThrow();
        round.setVotingAt(Instant.now());
        round.setCurrentState(State.VOTING);

        round = roundRepository.save(round);

        return RoundServiceImpl.mapRoundToDTO(round);
    }

    @Override
    @Transactional
    public RoundDTO voted(RoundDTO roundDTO, Long playerId) {

        Round round = roundRepository.findById(roundDTO.getId()).orElseThrow();
        Player player = playerRepository.findById(playerId).orElseThrow();
        List<Geodata> oldGeodata = round.getGeodata();
        List<GeodataDTO> newGeodata = roundDTO.getGeodata();

        if (!round.getPlayersVoted().contains(player)) {
            round.addPlayerVoted(player);

            List<Geodata> updatedGeodata = new ArrayList<>();

            for (int i = 0; i < oldGeodata.size(); i++) {
                Geodata oldData = oldGeodata.get(i);
                GeodataDTO newData = newGeodata.get(i);
                oldData.setUpvote(oldData.getUpvote() + newData.getUpvote());
                oldData.setFlag(oldData.isFlagged() || newData.isFlagged());
                oldData.setCounterFlag(oldData.getCounterFlag() + newData.getCounterFlag());
                geodataRepository.save(oldData);
                updatedGeodata.add(oldData);
            }

            round.setGeodata(updatedGeodata);
            round = roundRepository.save(round);

            // UPDATE ROUNDS IN LOBBY
            Lobby lobby = lobbyRepository.findById(round.getLobby().getId()).orElseThrow();
            lobby = lobbyService.updateRounds(lobby, round);
            lobbyRepository.save(lobby);

            round = roundRepository.save(round);
        }

        return RoundServiceImpl.mapRoundToDTO(round);
    }

    @Override
    @Transactional
    public RoundDTO createPlayer(String code, RoundDTO roundDTO) {

        Player newPlayer = playerService.createPlayer(roundDTO.getPlayers().get(roundDTO.getPlayers().size() - 1));

        Round round = roundRepository.findById(roundDTO.getId()).orElseThrow();
        Lobby lobby = lobbyRepository.findById(round.getLobby().getId()).orElseThrow();

        round = roundService.pushPlayerToRound(round.getId(), newPlayer);
        round = roundRepository.save(round);

        lobby = lobbyService.updateRounds(lobby, round);
        lobby = lobbyService.pushPlayerToLobby(code, newPlayer);
        lobby = lobbyRepository.save(lobby);

        round = roundRepository.save(round);

        return RoundServiceImpl.mapRoundToDTO(round);
    }


    @Override
    @Transactional
    public RoundDTO deletePlayer(RoundDTO roundDTO, Long playerId) {

        Round round = roundRepository.findById(roundDTO.getId()).orElseThrow();
        Player player = playerRepository.findById(playerId).orElseThrow();
        Lobby lobby = lobbyRepository.findById(round.getLobby().getId()).orElseThrow();

        round = roundService.deletePlayerFromRound(round, playerId);
        round = roundRepository.save(round);

        lobby = lobbyService.updateRounds(lobby, round);
        lobby = lobbyService.deletePlayerFromLobby(lobby, playerId);
        lobby = lobbyRepository.save(lobby);

        playerService.deletePlayer(playerId);

        return RoundServiceImpl.mapRoundToDTO(round);
    }

    @Override
    @Transactional
    public Geodata savePosition(SavepositionDTO savepositionDTO) {

        System.out.println("savepositionDTO = " + savepositionDTO);

        Round round = roundRepository.findById(savepositionDTO.getRoundId()).orElseThrow();
        Player player = playerRepository.findById(savepositionDTO.getPlayerId()).orElseThrow();
        Term term = termRepository.findById(savepositionDTO.getTermId()).orElseThrow();

        Optional<Geodata> geodataOptional = geodataRepository.findByRoundAndPlayerAndTerm(round, player, term);

        if (geodataOptional.isEmpty()) {
            geodataOptional = Optional.of(new Geodata());
        }

        Geodata geodata = geodataOptional.get();

        geodata.setLatitude(savepositionDTO.getLatitude());
        geodata.setLongitude(savepositionDTO.getLongitude());
        geodata.setHeading(savepositionDTO.getHeading());
        geodata.setPitch(savepositionDTO.getPitch());
        geodata.setZoom(savepositionDTO.getZoom());

        //Geodata auslesen
        geodata.toString();

        geodata.setRound(round);
        geodata.setPlayer(player);
        geodata.setTerm(term);

        geodata = geodataRepository.save(geodata);

        System.out.println("geodata = " + geodata);

        round.addGeodata(geodata);
        roundRepository.save(round);

        return geodata;
    }


    private List<Term> testRandomTerms(){

        Term term = new Term("Man with bird");
        Term term1 = new Term("TV");
        Term term2 = new Term("Yellow Porsche");
        Term term3 = new Term("Waterfall");
        Term term4 = new Term("Basketball Court");

        return termRepository.saveAll(List.of(term, term1, term2, term3, term4));

    }

}
