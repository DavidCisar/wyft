package com.abschlussprojekt.wyftspring.websocket;

public class Message {
    String name;

    public Message(String name) {
        this.name = name;
    }

    public Message() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
