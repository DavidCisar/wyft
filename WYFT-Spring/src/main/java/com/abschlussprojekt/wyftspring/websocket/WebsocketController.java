package com.abschlussprojekt.wyftspring.websocket;

import com.abschlussprojekt.wyftspring.geodata.Geodata;
import com.abschlussprojekt.wyftspring.round.RoundDTO;
import com.abschlussprojekt.wyftspring.websocket.dto.SavepositionDTO;
import com.abschlussprojekt.wyftspring.websocket.dto.WebsocketMessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class WebsocketController {

    private final SimpMessagingTemplate template;
    private final WebSocketService webSocketService;

    @Autowired
    public WebsocketController(SimpMessagingTemplate template, WebSocketService webSocketService) {
        this.template = template;
        this.webSocketService = webSocketService;
    }

    // http://localhost:8080/ws/lobby/app/{code}/test
    @MessageMapping("{code}/test") // APP TARGET PATH /ws/lobby/app/{code}/test FOR SENDING STUFF
    public void test(@DestinationVariable String code, Message message) throws Exception{
        // EMITS TO EVERYBODY WHO IS SUBSCRIBED TO THIS PATH
        this.template.convertAndSend( "/ws/lobby/topic/" + code, new Message(message.name));
    }

    @MessageMapping("{code}/start")
    public void startRound(@DestinationVariable String code, RoundDTO roundDTO) throws Exception{
        RoundDTO preparedRound = webSocketService.startRound(roundDTO);
        this.template.convertAndSend("/ws/lobby/topic/" + code + "/start", preparedRound);
    }

    @MessageMapping("{code}/end")
    public void endRound(@DestinationVariable String code, RoundDTO roundDTO) throws Exception{
        RoundDTO preparedRound = webSocketService.endRound(roundDTO);
        this.template.convertAndSend("/ws/lobby/topic/" + code + "/end", preparedRound);
    }

    @MessageMapping("{code}/voting/{playerId}")
    public void endVotingSelf(@DestinationVariable String code, @DestinationVariable Long playerId,RoundDTO roundDTO) throws Exception{
        RoundDTO preparedRound = webSocketService.voted(roundDTO, playerId);
        this.template.convertAndSend("/ws/lobby/topic/" + code + "/voting", preparedRound);
    }

    @MessageMapping("{code}/createPlayer")
    public void createPlayer(@DestinationVariable String code, RoundDTO roundDTO) throws Exception {
        RoundDTO preparedRound = webSocketService.createPlayer(code, roundDTO);
        this.template.convertAndSend("/ws/lobby/topic/" + code + "/createPlayer", preparedRound);
    }

    @MessageMapping("{code}/deletePlayer/{playerId}")
    public void deletePlayer(@DestinationVariable String code, @DestinationVariable Long playerId, RoundDTO roundDTO) throws Exception{
        RoundDTO preparedRound = webSocketService.deletePlayer(roundDTO, playerId);
        this.template.convertAndSend("/ws/lobby/topic/" + code + "/deletePlayer", preparedRound);
    }

    @MessageMapping("{code}/found")
    public void foundTerm(@DestinationVariable String code, SavepositionDTO savepositionDTO){

        System.out.println("savepositionDTO = " + savepositionDTO);

        Geodata geodataDTO = webSocketService.savePosition(savepositionDTO);

        WebsocketMessageDTO websocketMessageDTO = new WebsocketMessageDTO(
                (geodataDTO.getPlayer().getName() + " found " + geodataDTO.getTerm().getTerm()),
                "#3E8467"
        );

        this.template.convertAndSend("/ws/lobby/topic/" + code + "/message", websocketMessageDTO);
    }

}
