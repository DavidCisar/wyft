package com.abschlussprojekt.wyftspring.websocket.dto;

import java.math.BigDecimal;

public class SavepositionDTO {

    private Double longitude;
    private Double latitude;
    private Double heading;
    private Double pitch;
    private Double zoom;

    private Long playerId;
    private Long termId;
    private Long roundId;

    public SavepositionDTO() {
    }

    public SavepositionDTO(Double longitude, Double latitude, Double heading, Double pitch, Double zoom, Long playerId, Long termId, Long roundId) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.heading = heading;
        this.pitch = pitch;
        this.zoom = zoom;
        this.playerId = playerId;
        this.termId = termId;
        this.roundId = roundId;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Long getTermId() {
        return termId;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public Long getRoundId() {
        return roundId;
    }

    public void setRoundId(Long roundId) {
        this.roundId = roundId;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }

    public Double getPitch() {
        return pitch;
    }

    public void setPitch(Double pitch) {
        this.pitch = pitch;
    }

    public Double getZoom() {
        return zoom;
    }

    public void setZoom(Double zoom) {
        this.zoom = zoom;
    }

    @Override
    public String toString() {
        return "SavepositionDTO{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", heading=" + heading +
                ", pitch=" + pitch +
                ", zoom=" + zoom +
                ", playerId=" + playerId +
                ", termId=" + termId +
                ", roundId=" + roundId +
                '}';
    }
}
