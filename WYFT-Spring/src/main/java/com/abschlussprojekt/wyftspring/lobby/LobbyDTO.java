package com.abschlussprojekt.wyftspring.lobby;

import com.abschlussprojekt.wyftspring.player.PlayerDTO;
import com.abschlussprojekt.wyftspring.round.RoundDTO;

import java.util.List;

public class LobbyDTO {

    private Long id;
    private String code;
    private List<PlayerDTO> players;
    private List<RoundDTO> rounds;

    public LobbyDTO(
            Long id,
            String code,
            List<PlayerDTO> players,
            List<RoundDTO> rounds){
        this.id = id;
        this.code = code;
        this.players = players;
        this.rounds = rounds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<PlayerDTO> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerDTO> players) {
        this.players = players;
    }

    public List<RoundDTO> getRounds() {
        return rounds;
    }

    public void setRounds(List<RoundDTO> rounds) {
        this.rounds = rounds;
    }
}
