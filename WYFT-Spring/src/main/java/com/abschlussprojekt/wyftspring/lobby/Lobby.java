package com.abschlussprojekt.wyftspring.lobby;

import com.abschlussprojekt.wyftspring.player.Player;
import com.abschlussprojekt.wyftspring.round.Round;

import javax.persistence.*;
import java.util.List;

@Entity
public class Lobby {
    @Id
    @GeneratedValue
    private Long id;
    private String code;

    @OneToMany(mappedBy = "lobby")
    private List<Player> players;

    @OneToMany(mappedBy = "lobby")
    private List<Round> rounds;

    public Lobby() {}

    public Lobby(String code, List<Player> players, List<Round> rounds) {
        this.code = code;
        this.players = players;
        this.rounds = rounds;
    }

    public void addRound(Round round){
        this.rounds.add(round);
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    @Override
    public String toString() {
        return "Lobby{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", players=" + players +
                ", rounds=" + rounds +
                '}';
    }

    public void addPlayer(Player newPlayer) {
        this.players.add(newPlayer);
    }

    public int indexRound(Long roundId) {
        int index = -1;
        for (int i = 0; i < this.rounds.size(); i++) {
            if (this.rounds.get(i).getId() == roundId) {
                index = i;
            }
        }
        return index;
    }
}
