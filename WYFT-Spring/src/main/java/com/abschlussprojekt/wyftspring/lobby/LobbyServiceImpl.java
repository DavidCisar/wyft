package com.abschlussprojekt.wyftspring.lobby;

import com.abschlussprojekt.wyftspring.extras.Helpers;
import com.abschlussprojekt.wyftspring.player.Player;
import com.abschlussprojekt.wyftspring.player.PlayerRepository;
import com.abschlussprojekt.wyftspring.player.PlayerServiceImpl;
import com.abschlussprojekt.wyftspring.round.Round;
import com.abschlussprojekt.wyftspring.round.RoundRepository;
import com.abschlussprojekt.wyftspring.round.RoundServiceImpl;
import com.abschlussprojekt.wyftspring.term.Term;
import com.abschlussprojekt.wyftspring.term.TermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LobbyServiceImpl implements LobbyService{

    private final LobbyRepository lobbyRepository;
    private final RoundRepository roundRepository;
    private final PlayerRepository playerRepository;
    private final TermService termService;

    @Autowired
    public LobbyServiceImpl(LobbyRepository lobbyRepository, RoundRepository roundRepository, PlayerRepository playerRepository, TermService termService) {
        this.lobbyRepository = lobbyRepository;
        this.roundRepository = roundRepository;
        this.playerRepository = playerRepository;
        this.termService = termService;
    }

    @Override
    public LobbyDTO createLobby(int timeLimitRound, int timeLimitVoting, int amountOfTerms) {

        String code = Helpers.generateCode(6);

        Lobby lobby = new Lobby(
             code,
             new ArrayList<>(),
             new ArrayList<>()
        );

        lobby = lobbyRepository.save(lobby);

        List<Term> terms = termService.getRandomTerms(amountOfTerms);
        Player admin = playerRepository.save(new Player("placeholder", "ipaddress", true, Instant.now()));

        Round round = new Round(
                Instant.now(),
                Instant.now(),
                timeLimitRound,
                timeLimitVoting,
                amountOfTerms,
                admin,
                new ArrayList<>(),
                new ArrayList<>(),
                lobby,
                terms,
                new ArrayList<>()
        );
        round = roundRepository.save(round);

        lobby.addRound(round);
        lobbyRepository.save(lobby);

        return mapLobbyToDTO(lobby);

    }

    @Override
    public LobbyDTO findLobbyByCode(String code) {
        Lobby lobby = lobbyRepository.findLobbyByCode(code).orElseThrow();
        System.out.println("lobby = " + lobby);
        return mapLobbyToDTO(lobby);
    }

    @Override
    public Lobby pushPlayerToLobby(String code, Player newPlayer) {
        Lobby lobby = lobbyRepository.findLobbyByCode(code).orElseThrow();
        lobby.addPlayer(newPlayer);
        return lobbyRepository.save(lobby);
    }

    @Override
    public Lobby deletePlayerFromLobby(Lobby lobby, Long playerId) {
        int index = -1;
        for (int i = 0; i < lobby.getPlayers().size(); i++) {
            if (lobby.getPlayers().get(i).getId() == playerId) {
                index = i;
            }
        }
        if (index != -1) {
            lobby.getPlayers().remove(index);
        }
        return lobby;
    }

    @Override
    public Lobby updateRounds(Lobby lobby, Round round) {
        int index = -1;
        for (int i = 0; i < lobby.getRounds().size(); i++) {
            if (lobby.getRounds().get(i).getId() ==  round.getId()) {
                index = i;
            }
        }
        lobby.getRounds().set(index, round);
        return lobby;
    }

    public static LobbyDTO mapLobbyToDTO(Lobby lobby){
        return new LobbyDTO(
                lobby.getId(),
                lobby.getCode(),
                lobby.getPlayers().stream().map(PlayerServiceImpl::mapPlayerToDTO).collect(Collectors.toList()),
                lobby.getRounds().stream().map(RoundServiceImpl::mapRoundToDTO).collect(Collectors.toList())
        );
    }

}
