package com.abschlussprojekt.wyftspring.lobby;

import com.abschlussprojekt.wyftspring.player.Player;
import com.abschlussprojekt.wyftspring.round.Round;

public interface LobbyService {
    LobbyDTO createLobby(int timeLimitRound, int timeLimitVoting, int amountOfTerms);
    LobbyDTO findLobbyByCode(String code);
    Lobby pushPlayerToLobby(String code, Player newPlayer);
    Lobby deletePlayerFromLobby(Lobby lobby, Long playerId);
    Lobby updateRounds(Lobby lobby, Round round);
}


