package com.abschlussprojekt.wyftspring.lobby;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/lobby")
public class LobbyController {

    private final LobbyService lobbyService;

    @Autowired
    public LobbyController(LobbyService lobbyService) {
        this.lobbyService = lobbyService;
    }

    @GetMapping("/create")
    public ResponseEntity<LobbyDTO> createLobby(@RequestParam("tlr") int timeLimitRound,
                                                @RequestParam("tlv") int timeLimitVoting,
                                                @RequestParam("aot") int amountOfTerms){
        LobbyDTO lobbyDTO = lobbyService.createLobby(timeLimitRound, timeLimitVoting, amountOfTerms);
        return new ResponseEntity<>(lobbyDTO, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<LobbyDTO> getLobby(@RequestParam(required = false) String code){
        LobbyDTO lobbyDTO = lobbyService.findLobbyByCode(code);
        return new ResponseEntity<>(lobbyDTO, HttpStatus.OK);
    }

}
