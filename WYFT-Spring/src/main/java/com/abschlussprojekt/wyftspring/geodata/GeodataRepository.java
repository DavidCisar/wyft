package com.abschlussprojekt.wyftspring.geodata;

import com.abschlussprojekt.wyftspring.player.Player;
import com.abschlussprojekt.wyftspring.round.Round;
import com.abschlussprojekt.wyftspring.term.Term;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GeodataRepository extends JpaRepository<Geodata, Long> {
    Optional<Geodata> findByRoundAndPlayerAndTerm(Round round, Player player, Term term);
}
