package com.abschlussprojekt.wyftspring.geodata;

import com.abschlussprojekt.wyftspring.player.Player;
import com.abschlussprojekt.wyftspring.round.Round;
import com.abschlussprojekt.wyftspring.term.Term;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Geodata {

    @Id
    @GeneratedValue
    private Long id;

    private Double longitude;
    private Double latitude;

    private Double heading;
    private Double pitch;
    private Double zoom;

    private int upvote = 0;
    private int counterFlag = 0;
    private boolean flagged = false;

    @JsonIgnore
    @ManyToOne
    private Player player;

    @JsonIgnore
    @ManyToOne
    private Term term;

    @JsonIgnore
    @ManyToOne
    private Round round;

    public Geodata() {}

    public Geodata(Double longitude, Double latitude, Double heading, Double pitch, Double zoom) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.heading = heading;
        this.pitch = pitch;
        this.zoom = zoom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }

    public Double getPitch() {
        return pitch;
    }

    public void setPitch(Double pitch) {
        this.pitch = pitch;
    }

    public Double getZoom() {
        return zoom;
    }

    public void setZoom(Double zoom) {
        this.zoom = zoom;
    }

    public int getUpvote() {
        return upvote;
    }

    public void setUpvote(int upvote) {
        this.upvote = upvote;
    }

    public int getCounterFlag() {
        return counterFlag;
    }

    public void setCounterFlag(int counterFlag) {
        this.counterFlag = counterFlag;
    }

    public boolean isFlagged() {
        return flagged;
    }

    public void setFlag(boolean flag) {
        this.flagged = flag;
    }

    @Override
    public String toString() {
        return "Geodata{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", heading=" + heading +
                ", pitch=" + pitch +
                ", zoom=" + zoom +
                '}';
    }
}
