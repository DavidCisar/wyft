package com.abschlussprojekt.wyftspring.geodata;

import com.abschlussprojekt.wyftspring.player.PlayerDTO;
import com.abschlussprojekt.wyftspring.term.TermDTO;


public class GeodataDTO {

    private Long id;

    private Double longitude;
    private Double latitude;

    private Double heading;
    private Double pitch;
    private Double zoom;

    private int upvote;
    private int counterFlag;
    private boolean isFlagged;

    private PlayerDTO player;
    private TermDTO term;

    public GeodataDTO(Long id, Double longitude, Double latitude, Double heading, Double pitch, Double zoom,
                      int upvote, int counterFlag, boolean isFlagged, PlayerDTO player, TermDTO term) {
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
        this.heading = heading;
        this.pitch = pitch;
        this.zoom = zoom;
        this.upvote = upvote;
        this.counterFlag = counterFlag;
        this.isFlagged = isFlagged;
        this.player = player;
        this.term = term;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public PlayerDTO getPlayer() {
        return player;
    }

    public void setPlayer(PlayerDTO player) {
        this.player = player;
    }

    public TermDTO getTerm() {
        return term;
    }

    public void setTerm(TermDTO term) {
        this.term = term;
    }

    public int getUpvote() {
        return upvote;
    }

    public void setUpvote(int upvote) {
        this.upvote = upvote;
    }

    public int getCounterFlag() {
        return counterFlag;
    }

    public void setCounterFlag(int counterFlag) {
        this.counterFlag = counterFlag;
    }

    public boolean isFlagged() {
        return isFlagged;
    }

    public void setFlagged(boolean flagged) {
        isFlagged = flagged;
    }

    public Double getHeading() {
        return heading;
    }

    public void setHeading(Double heading) {
        this.heading = heading;
    }

    public Double getPitch() {
        return pitch;
    }

    public void setPitch(Double pitch) {
        this.pitch = pitch;
    }

    public Double getZoom() {
        return zoom;
    }

    public void setZoom(Double zoom) {
        this.zoom = zoom;
    }
}
