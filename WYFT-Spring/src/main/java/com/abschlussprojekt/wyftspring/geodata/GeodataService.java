package com.abschlussprojekt.wyftspring.geodata;

import java.util.List;

import com.abschlussprojekt.wyftspring.round.RoundDTO;

public interface GeodataService {
    List<GeodataDTO> findAllByRound(Long roundId);
    List<GeodataDTO> findAll();
    List<Geodata> saveGeodata(RoundDTO roundDTO);
}
