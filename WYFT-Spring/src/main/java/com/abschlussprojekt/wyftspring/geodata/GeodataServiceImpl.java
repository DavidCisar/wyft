package com.abschlussprojekt.wyftspring.geodata;

import com.abschlussprojekt.wyftspring.player.PlayerServiceImpl;
import com.abschlussprojekt.wyftspring.round.Round;
import com.abschlussprojekt.wyftspring.round.RoundRepository;
import com.abschlussprojekt.wyftspring.round.RoundDTO;
import com.abschlussprojekt.wyftspring.term.TermServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GeodataServiceImpl implements GeodataService{

    private final RoundRepository roundRepository;
    private final GeodataRepository geodataRepository;

    @Autowired
    public GeodataServiceImpl(RoundRepository roundRepository, GeodataRepository geodataRepository) {
        this.roundRepository = roundRepository;
        this.geodataRepository = geodataRepository;
    }

    public List<Geodata> saveGeodata(RoundDTO roundDTO) {
        List<Geodata> geodataList = new ArrayList<>();
        for (GeodataDTO geodataDTO : roundDTO.getGeodata()) {
            Geodata geodata = geodataRepository.findById(geodataDTO.getId()).orElseThrow();
            int currentUpVote = geodata.getUpvote();
            geodata.setUpvote(currentUpVote + geodataDTO.getUpvote());
            if (geodataDTO.isFlagged()){
                int currentCounterFlag = geodata.getCounterFlag();
                geodata.setCounterFlag(currentCounterFlag + geodataDTO.getCounterFlag());
                geodata.setFlag(true);
            }
            Geodata geodataSaved = geodataRepository.save(geodata);
            geodataList.add(geodataSaved);
        }
        return geodataList;
    }

    public static GeodataDTO mapGeodataToDTO(Geodata geodata){
        return new GeodataDTO(
                geodata.getId(),
                geodata.getLongitude(),
                geodata.getLatitude(),
                geodata.getHeading(),
                geodata.getPitch(),
                geodata.getZoom(),
                geodata.getUpvote(),
                geodata.getCounterFlag(),
                geodata.isFlagged(),
                PlayerServiceImpl.mapPlayerToDTO(geodata.getPlayer()),
                TermServiceImpl.mapTermToDTO(geodata.getTerm())
        );
    }

    @Override
    public List<GeodataDTO> findAllByRound(Long roundId) {
        Round round = roundRepository.findById(roundId).orElseThrow();
        return round.getGeodata().stream().map(GeodataServiceImpl::mapGeodataToDTO).collect(Collectors.toList());
    }

    @Override
    public List<GeodataDTO> findAll() {
        List<Geodata> geodata = geodataRepository.findAll();
        return geodata.stream().map(GeodataServiceImpl::mapGeodataToDTO).collect(Collectors.toList());
    }
}
