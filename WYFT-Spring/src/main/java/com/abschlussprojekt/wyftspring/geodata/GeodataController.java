package com.abschlussprojekt.wyftspring.geodata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/geodata")
public class GeodataController {

    private final GeodataService geodataService;

    @Autowired
    public GeodataController(GeodataService geodataService) {
        this.geodataService = geodataService;
    }

    @GetMapping
    public ResponseEntity<List<GeodataDTO>> getRoundGeodata(@RequestParam Long roundId){
        List<GeodataDTO> geodata = geodataService.findAllByRound(roundId);
        return new ResponseEntity<>(geodata, HttpStatus.OK);
    }

    @GetMapping("/history")
    public ResponseEntity<List<GeodataDTO>> getAllGeodata() {
        List<GeodataDTO> geodata = geodataService.findAll();
        return new ResponseEntity<>(geodata, HttpStatus.OK);
    }


}
