package com.abschlussprojekt.wyftspring;

import com.abschlussprojekt.wyftspring.geodata.Geodata;
import com.abschlussprojekt.wyftspring.geodata.GeodataRepository;
import com.abschlussprojekt.wyftspring.lobby.Lobby;
import com.abschlussprojekt.wyftspring.lobby.LobbyRepository;
import com.abschlussprojekt.wyftspring.lobby.LobbyService;
import com.abschlussprojekt.wyftspring.player.Player;
import com.abschlussprojekt.wyftspring.player.PlayerRepository;
import com.abschlussprojekt.wyftspring.round.Round;
import com.abschlussprojekt.wyftspring.round.RoundRepository;
import com.abschlussprojekt.wyftspring.term.Term;
import com.abschlussprojekt.wyftspring.term.TermRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


@SpringBootApplication
public class WyftSpringApplication {

    private final LobbyRepository lobbyRepository;
    private final PlayerRepository playerRepository;
    private final RoundRepository roundRepository;
    private final TermRepository termRepository;
    private final GeodataRepository geodataRepository;

    public WyftSpringApplication(LobbyRepository lobbyRepository, PlayerRepository playerRepository, RoundRepository roundRepository, TermRepository termRepository, GeodataRepository geodataRepository) {
        this.lobbyRepository = lobbyRepository;
        this.playerRepository = playerRepository;
        this.roundRepository = roundRepository;
        this.termRepository = termRepository;
        this.geodataRepository = geodataRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(WyftSpringApplication.class, args);
    }

    @PostConstruct
    public void dummyData(){

        Lobby dummyLobby = lobbyRepository.save(new Lobby("654123",new ArrayList<Player>(),new ArrayList<Round>()));

        List<Player> dummyPlayerList = new ArrayList<>();

        Player dummyPlayer1 = playerRepository.save(new Player("David","192.168.0.1.", true, Instant.now()));
        Player dummyPlayer2 = playerRepository.save(new Player("Ibrahim", "192.168.10.254", true,Instant.now()));
        Player dummyPlayer3 = playerRepository.save(new Player("Christian", "192.170.11.214", true,Instant.now()));

        dummyPlayerList.add(dummyPlayer1);
        dummyPlayerList.add(dummyPlayer2);
        dummyPlayerList.add(dummyPlayer3);

        dummyLobby.setPlayers(dummyPlayerList);

        List<Term> dummyTerms = new ArrayList<>();

        Term dummyTerm1 = termRepository.save(new Term("AW Academy"));
        Term dummyTerm2 = termRepository.save(new Term("Ferry"));
        Term dummyTerm3 = termRepository.save(new Term("Telephone Cell"));
        Term dummyTerm4 = termRepository.save(new Term("Stadion"));
        Term dummyTerm5 = termRepository.save(new Term("Sheep"));


        dummyTerms.add(dummyTerm1);
        dummyTerms.add(dummyTerm2);
        dummyTerms.add(dummyTerm3);
        dummyTerms.add(dummyTerm4);
        dummyTerms.add(dummyTerm5);

        Round dummyRound = roundRepository.save(new Round(
                Instant.now(),
                Instant.now(),
                1800,
                600,
                5,
                dummyPlayer1,
                dummyPlayerList,
                dummyPlayerList,
                dummyLobby,
                dummyTerms,
                new ArrayList<>()
        ));

        List<Round> dummyRoundList = new ArrayList<>();
        dummyRoundList.add(dummyRound);

        dummyLobby.setRounds(dummyRoundList);
        lobbyRepository.save(dummyLobby);

        List<Geodata> dummyGeodataList = new ArrayList<>();


        Geodata dummyGeodata1 = new Geodata(9.987631658114976, 53.54905840990458, 143.8786581670849, 12.788453692221779, 1.3715607474702975);
        dummyGeodata1.setPlayer(dummyPlayer1);
        dummyGeodata1.setTerm(dummyTerm1);
        dummyGeodata1.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata1));


        Geodata dummyGeodata2 = new Geodata(-118.4968931669011, 34.0097695275936, 135.1322228849474, -2.1106622127930166, 1.0583218335131763);
        dummyGeodata2.setPlayer(dummyPlayer1);
        dummyGeodata2.setTerm(dummyTerm2);
        dummyGeodata2.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata2));


        Geodata dummyGeodata3 = new Geodata(-81.3119809, 34.0325296, 36.36204349170896, 1.8664877131939477, 1.0);
        dummyGeodata3.setPlayer(dummyPlayer1);
        dummyGeodata3.setTerm(dummyTerm3);
        dummyGeodata3.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata3));

        Geodata dummyGeodata4 = new Geodata(-81.3119809, 34.0325296, 36.36204349170896, 1.8664877131939477, 1.0);
        dummyGeodata4.setPlayer(dummyPlayer1);
        dummyGeodata4.setTerm(dummyTerm4);
        dummyGeodata4.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata4));

        Geodata dummyGeodata5 = new Geodata(-81.3119809, 34.0325296, 36.36204349170896, 1.8664877131939477, 1.0);
        dummyGeodata5.setPlayer(dummyPlayer1);
        dummyGeodata5.setTerm(dummyTerm5);
        dummyGeodata5.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata5));

        Geodata dummyGeodata6 = new Geodata(11.55688281387897, 48.14336681451833, 127.22728807086679, 11.22508828988687, 0.7189482652207981);
        dummyGeodata6.setPlayer(dummyPlayer2);
        dummyGeodata6.setTerm(dummyTerm1);
        dummyGeodata6.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata6));

        Geodata dummyGeodata7 = new Geodata(4.7780271, 52.9645634, 0.0, 0.0, 1.0);
        dummyGeodata7.setPlayer(dummyPlayer2);
        dummyGeodata7.setTerm(dummyTerm2);
        dummyGeodata7.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata7));

        Geodata dummyGeodata8 = new Geodata(-0.1231450928392249, 51.5122547176985, 299.356995467327, -8.58161142631974, 1.0);
        dummyGeodata8.setPlayer(dummyPlayer2);
        dummyGeodata8.setTerm(dummyTerm3);
        dummyGeodata8.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata8));

        Geodata dummyGeodata9 = new Geodata(8.83999888467266, 53.06706887292847, 233.62932204044722, 4.058708786189499, 0.0);
        dummyGeodata9.setPlayer(dummyPlayer2);
        dummyGeodata9.setTerm(dummyTerm4);
        dummyGeodata9.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata9));

        Geodata dummyGeodata10 = new Geodata(-81.3119809, 34.0325296, 36.36204349170896, 1.8664877131939477, 1.0);
        dummyGeodata10.setPlayer(dummyPlayer2);
        dummyGeodata10.setTerm(dummyTerm5);
        dummyGeodata10.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata10));

        Geodata dummyGeodata11 = new Geodata(-81.3119809, 34.0325296, 36.36204349170896, 1.8664877131939477, 1.0);
        dummyGeodata11.setPlayer(dummyPlayer3);
        dummyGeodata11.setTerm(dummyTerm1);
        dummyGeodata11.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata11));

        Geodata dummyGeodata12 = new Geodata(-81.3119809, 34.0325296, 36.36204349170896, 1.8664877131939477, 1.0);
        dummyGeodata12.setPlayer(dummyPlayer3);
        dummyGeodata12.setTerm(dummyTerm2);
        dummyGeodata12.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata12));

        Geodata dummyGeodata13 = new Geodata(-81.3119809, 34.0325296, 36.36204349170896, 1.8664877131939477, 1.0);
        dummyGeodata13.setPlayer(dummyPlayer3);
        dummyGeodata13.setTerm(dummyTerm3);
        dummyGeodata13.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata13));

        Geodata dummyGeodata14 = new Geodata(-81.3119809, 34.0325296, 36.36204349170896, 1.8664877131939477, 1.0);
        dummyGeodata14.setPlayer(dummyPlayer3);
        dummyGeodata14.setTerm(dummyTerm4);
        dummyGeodata14.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata14));

        Geodata dummyGeodata15 = new Geodata(-81.3119809, 34.0325296, 36.36204349170896, 1.8664877131939477, 1.0);
        dummyGeodata15.setPlayer(dummyPlayer3);
        dummyGeodata15.setTerm(dummyTerm5);
        dummyGeodata15.setRound(dummyRound);
        dummyGeodataList.add(geodataRepository.save(dummyGeodata15));

        dummyRound.setGeodata(dummyGeodataList);
        roundRepository.save(dummyRound);
    }

}
