package com.abschlussprojekt.wyftspring.term;

import com.abschlussprojekt.wyftspring.geodata.GeodataDTO;
import com.abschlussprojekt.wyftspring.round.RoundDTO;

import java.util.List;

public class TermDTO {

    private Long id;
    private String term;

    public TermDTO(
            Long id,
            String term){
        this.id = id;
        this.term = term;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

}
