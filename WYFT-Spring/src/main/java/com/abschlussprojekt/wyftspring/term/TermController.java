package com.abschlussprojekt.wyftspring.term;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api/term")
public class TermController {

    private final TermRepository termRepository;

    @Autowired
    public TermController(TermRepository termRepository) {
        this.termRepository = termRepository;
    }

    @GetMapping("/getAllTerms")
    public ResponseEntity<List<Term>> getTerms() {
        List<Term> allTerms = termRepository.findAll();
        for (Term term: allTerms) {
            System.out.println("term = " + term.getTerm());
        }
        return new ResponseEntity<>(allTerms, HttpStatus.OK);
    }

}
