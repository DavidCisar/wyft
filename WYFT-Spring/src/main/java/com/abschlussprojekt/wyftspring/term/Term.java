package com.abschlussprojekt.wyftspring.term;

import com.abschlussprojekt.wyftspring.geodata.Geodata;
import com.abschlussprojekt.wyftspring.round.Round;

import javax.persistence.*;
import java.util.List;

@Entity
public class Term {
    @Id
    @GeneratedValue
    private Long id;
    private String term;

    @OneToMany(mappedBy = "term")
    private List<Geodata> geodata;

    @ManyToMany
    private List<Round> rounds;

    public Term() {}

    public Term(String term) {
        this.term = term;
    }

    public Long getId() {
        return id;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public List<Geodata> getGeodata() {
        return geodata;
    }

    public void setGeodata(List<Geodata> geodata) {
        this.geodata = geodata;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }
}
