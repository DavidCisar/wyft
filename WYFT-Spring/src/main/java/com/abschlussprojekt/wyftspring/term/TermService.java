package com.abschlussprojekt.wyftspring.term;

import java.util.List;

public interface TermService {
    List<Term> getRandomTerms(int amountOfTerms);
}
