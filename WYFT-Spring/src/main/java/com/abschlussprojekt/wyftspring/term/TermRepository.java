package com.abschlussprojekt.wyftspring.term;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TermRepository extends JpaRepository<Term, Long> {
    Optional<Term> findTermById(Long id);
}
