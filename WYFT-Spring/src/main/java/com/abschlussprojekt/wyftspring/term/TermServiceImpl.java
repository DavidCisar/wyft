package com.abschlussprojekt.wyftspring.term;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TermServiceImpl implements  TermService{

    private final TermRepository termRepository;

    @Autowired
    public TermServiceImpl(TermRepository termRepository){
        this.termRepository = termRepository;
        prefillTermTable();
    }

    public void prefillTermTable() {
        termRepository.save(new Term("Yellow Porsche"));
        termRepository.save(new Term("Cat"));
        termRepository.save(new Term("Hat"));
        termRepository.save(new Term("Graffiti"));
        termRepository.save(new Term("Trash can"));
        termRepository.save(new Term("Lamp post"));
        termRepository.save(new Term("Mailbox"));
        termRepository.save(new Term("Flag"));
        termRepository.save(new Term("Tree"));
        termRepository.save(new Term("Man with green t-shirt"));
        termRepository.save(new Term("Food truck"));
        termRepository.save(new Term("Dog"));
        termRepository.save(new Term("Pigeon"));
        termRepository.save(new Term("Street art"));
        termRepository.save(new Term("Umbrella"));
        termRepository.save(new Term("Playground"));
        termRepository.save(new Term("Police car"));
        termRepository.save(new Term("Supermarket"));
        termRepository.save(new Term("Gas station"));
        termRepository.save(new Term("Ambulance"));
        termRepository.save(new Term("Clock"));
        termRepository.save(new Term("Underground station"));

    }

    public static TermDTO mapTermToDTO(Term term){
        return new TermDTO(
                term.getId(),
                term.getTerm()
        );
    }

    public List<Term> getRandomTerms(int amountOfTerms) {
        List<Term> returnList = new ArrayList<>();
        List<Term> allTerms = termRepository.findAll();
        while ( returnList.size() < amountOfTerms){
            Term selectedTerm = allTerms.get((int) ((Math.random() * (allTerms.size()))));
                    if (!returnList.contains(selectedTerm)) {
                        returnList.add(selectedTerm);
                    }
        }
        return returnList;
    }
}
