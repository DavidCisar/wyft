package com.abschlussprojekt.wyftspring;

public enum State {
    NOTSTARTED,
    STARTED,
    VOTING
}
